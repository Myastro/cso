
class Passenger {

	static idCounter = 0;
	_id = null;
	name = null;
	enable = null;
	startCity = null;
	endCity = null;
	distance = null;

	constructor(o) {
		this._id = Passenger.idCounter++;
		this.enable = o.enable || true;
		this.startCity = o.startCity;
		this.endCity = o.endCity;
		this.name = o.name;
		this.distance = o.endCity.distance - o.startCity.distance;
	}

	getName() {
		return this.name;
	}

	setName(n) {
		this.name = n;
	}

	getStartCity() {
		return this.startCity;
	}

	setStartCity(v) {
		this.startCity = v;
	}

	getEndCity() {
		return this.endCity;
	}

	setEndCity(v) {
		this.endCity = v;
	}

	static fromObject(o) {

		return new Passenger(o);

	}

}

export default Passenger;
