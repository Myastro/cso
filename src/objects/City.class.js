class City {

	static idCounter = 0;
	distance = null;
	_id = null;
	name = null;

	constructor(o) {
		this._id = City.idCounter++;
		this.name = o.name;
		this.distance = parseFloat(o.distance);
	}

	getName() {
		return this.name;
	}

	setName(n) {
		this.name = n;
	}

	static fromObject(o) {

		return new City(o);
	}
}

export default City;
