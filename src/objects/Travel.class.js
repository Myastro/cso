import City from './City.class';

class Travel {

	cities = [];
	passengers = [];
	costs = [];

	constructor(cities = [], costs = []){
		this.cities = this.cities.concat(cities);
		this.sortCities();
		this.costs = this.costs.concat(costs);
	}

	addCity(c) {

		this.cities.push(new City(c));

		this.sortCities();
		if(this.cities.length > 1) {
			this.costs.push(100);
		}
	}

	sortCities() {
		this.cities.sort((a, b) => a.distance < b.distance ? -1 : 1);

	}
	getCities() {
		return this.cities;
	}

	getCosts() {
		return this.costs;
	}

	removeCity(c) {
		if(this.cities.length > 1 ) {
			this.costs.pop();
		}
		this.cities = this.cities.filter( _ => c._id !== _._id);

	}

	setCost(c, i) {
		this.costs[i] = c;
	}

	static fromObject(o) {
		let cities;
		let costs;

		if(o.cities) {
			cities = o.cities.map( _ => new City(_));
		}

		if(o.costs) {
			costs = o.costs;
		}

		return new Travel(cities, costs);
	}
}

export default Travel;
