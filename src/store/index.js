import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import passenger from './modules/passenger';
import travel from './modules/travel';

const store = new Vuex.Store({
	modules: {
		passenger,
		travel
	}
});

export default store;
