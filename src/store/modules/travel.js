import City from '../../objects/City.class';
import Travel from '../../objects/Travel.class';

import passenger from './passenger'
import * as types from '../mutationsTypes';


const defaultCost = 10;
const defaultTravel = new Travel([
	new City({name: 'Toulouse', distance: 0}),
	new City({name: 'Narbonne', distance: 100}),
	new City({name: 'Vienne', distance: 450}),
	new City({name: 'Lyon', distance: 500}),
	new City({name: 'Munich', distance: 1450})
]);

const state = {
	cost: defaultCost,
	travel: new Travel()
};

const getters = {
	totalCost: () => {
		let c = 0;
		c = Math.max.apply(Math, passenger.getters.enabledPassengers().map(function(o) { return o.distance * state.cost; }));
		return c;
	},
	sumAloneCosts: () => {
		let c = 0;
		passenger.getters.enabledPassengers().forEach((_) => { c += (_.distance * state.cost)});
		return c;
	},
	cost: () => state.cost,
	cities: () => state.travel.getCities(),
	travel: () => state.travel
};

const actions = {
	city_addOne: async({commit}, c) => {
		commit(types.ADD_CITY, c);
	},
	city_moveBack: async({commit}, c, index) => {
		commit(types.CITY_MOVE_BACK, c, index);
	},
	city_moveNext: async({commit}, c, index) => {
		commit(types.CITY_MOVE_NEXT, c, index);
	},
	city_removeOne: async({commit}, c) => {
		commit(types.REMOVE_CITY, c);
	},
	load_travel: async({commit}) => {
		commit(types.LOAD_TRAVEL);
	},
	travel_reset: async({commit}) => {
		commit(types.RESET_TRAVEL);
	},
	set_cost: async({commit}) => {
		commit(types.SET_COST);
	}
};
const mutations = {
	[types.LOAD_TRAVEL] (state) {
		state.travel = Travel.fromObject(JSON.parse(localStorage.getItem('travel')));
		let c = localStorage.getItem('cost');
		if(c) {
			state.cost = parseFloat(c);
		}
	},
	[types.ADD_CITY] (state, c) {
		state.travel.addCity(c);
		localStorage.setItem('travel', JSON.stringify(state.travel));
	},
	[types.REMOVE_CITY] (state,c) {
		state.travel.removeCity(c);
		localStorage.setItem('travel', JSON.stringify(state.travel));
	},
	[types.RESET_TRAVEL] (state) {
		state.travel = defaultTravel;
		state.cost = defaultCost;
		localStorage.setItem('travel', JSON.stringify(state.travel));
	},
	[types.SET_COST] (state, cost) {
		state.cost = parseFloat(cost);
		localStorage.setItem('cost', cost);
	},
	[types.CITY_MOVE_BACK] (state, index) {
		state.travel.moveCityBack(index);
	},
	[types.CITY_MOVE_NEXT] (state, index) {
		state.travel.moveCityNext(index);
	}

};

export default {
	getters,
	state,
	actions,
	mutations
}
