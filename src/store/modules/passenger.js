import Passenger from '../../objects/Passenger.class';
import travel from './travel';
import * as types from '../mutationsTypes';


const state = {
	passengers: []
};

const getters = {
	separationCost: (state) => (passenger) => separationCost(state, passenger),
	proportionnalCost: (state) => (passenger) => proportionnalCost(state, passenger),
	enabledPassengers: () => state.passengers.filter( _ => _.enable === true),
	couplePassenger: () => couplePassenger().filter( _ => _.length === 2),
	triplePassenger: () => triplePassenger().filter( _ => _.length === 3),
	quatuorPassenger: () => quatuorPassenger().filter( _ => _.length === 4),
	passengers: () => state.passengers.sort((a, b) => a.distance < b.distance ? -1 : 1),
	uniqueDistance: () => Array.from(new Set(state.passengers.map((_) => _.distance)))
};

const actions = {
	passenger_addOne: async({commit}, p) => {
		commit(types.ADD_PASSENGER, new Passenger(p));
	},
	passenger_setOne: async({commit}, p) => {
		commit(types.SET_PASSENGER, new Passenger(p));
	},
	passenger_deleteOne: async({commit}, p) => {
		commit(types.REMOVE_PASSENGER, p)
	},
	load_passengers: async({commit}) => {
		commit(types.LOAD_PASSENGERS);
	},
	passengers_resetAll: async ({commit}) => {
		commit(types.RESET_PASSENGERS);
	}
};
const mutations = {
	[types.LOAD_PASSENGERS] (state) {
		let passengers = JSON.parse(localStorage.getItem('passengers'));
		if(passengers) {
			state.passengers = passengers.map(_ => Passenger.fromObject(_));
		}
	},
	[types.ADD_PASSENGER] (state, passenger) {
		state.passengers.push(passenger);
		localStorage.setItem('passengers', JSON.stringify(state.passengers));
	},
	[types.SET_PASSENGER] (state, passenger) {
		state.passengers = state.passengers.map(_ => _._id === passenger._id ? passenger : _);
		localStorage.setItem('passengers', JSON.stringify(state.passengers));

	},
	[types.REMOVE_PASSENGER] (state, passenger) {
		state.passengers = state.passengers.filter(_ => _._id !== passenger._id);
		localStorage.setItem('passengers', JSON.stringify(state.passengers));
	},
	[types.RESET_PASSENGERS] (state) {
		state.passengers = [];
		localStorage.setItem('passengers', JSON.stringify(state.passengers));
	}
};

export default {
	getters,
	state,
	actions,
	mutations
}


function couplePassenger(a1 = state.passengers, a2 = state.passengers) {


	let tmp = [];
	for(let j = 0; j < a1.length; j++) {
		for(let k = j + 1; k < a2.length; k++) {
			tmp.push(fusion(a1[j], a2[k]));
		}
	}

	return tmp;

}

function triplePassenger() {


	let tmp = [];
	for(let i = 0; i < state.passengers.length; i ++) {
		for(let j = i + 1; j < state.passengers.length; j++) {
			for(let k = j + 1; k < state.passengers.length; k++) {
				tmp.push(Array.from(new Set([state.passengers[i], state.passengers[j], state.passengers[k]])));
			}
		}
	}


	return tmp;

}

function quatuorPassenger() {


	let tmp = [];
	for(let i = 0; i < state.passengers.length; i++) {
		for(let j = i + 1; j < state.passengers.length; j++) {
			for(let k = j + 1; k < state.passengers.length; k++) {
				for(let l = k + 1; l < state.passengers.length; l++) {
					tmp.push(Array.from(new Set([state.passengers[i], state.passengers[j], state.passengers[k], state.passengers[l]])));
				}
			}
		}
	}


	return tmp;

}

function fusion(p1, p2) {



	if(Array.isArray(p1) ) {
		let s1 = new Set(p1);
		if(Array.isArray(p2)) {

			let s2 = new Set(p2);

			for(let i of s2) {
				s1.add(i)
			}
		} else {
			s1.add(p2);
		}
		return Array.from(s1);

	}

	let ret = new Set();

	ret.add(p1);
	ret.add(p2);

	return Array.from(ret);
}


function separationCost (state, passenger) {

		let passengers = state.passengers.filter( _ => _.enable === true);

		let uniqueDistance = Array.from(new Set(state.passengers.map((_) => _.distance)));

		let indexPassenger = passengers.findIndex(_ => _.name === passenger.name);
		let lastPassenger = passengers.findIndex( _ => _.distance > passenger.distance);

		if( indexPassenger === 0) {
			return (travel.getters.cost() * passenger.distance) / (passengers.length);
		}

		let denomArray = [];

		denomArray[0] = passengers.length;
		let count = passengers.length;

		for(let i = 1; i < passengers.length; i++) {
			if(passengers[i].distance === passengers[i - 1].distance) {
				denomArray[i] = count;
			} else {
				denomArray[i] = passengers.length - i;
				count = passengers.length - i;
			}
		}

		let costs = [];
		let indexDistance = 0;
		for(let i = 1; i < passengers.length; i++) {

			if(indexDistance === 0 && (passengers[i].distance === uniqueDistance[indexDistance])) {
				costs[i] = (uniqueDistance[indexDistance] * travel.getters.cost()) / denomArray[i];
			} else if( passengers[i].distance === uniqueDistance[indexDistance]) {
				costs[i] = (travel.getters.cost() * (uniqueDistance[indexDistance] - uniqueDistance[indexDistance-1])) /denomArray[i];
			} else {
				indexDistance ++;
				costs[i] = (travel.getters.cost() * (uniqueDistance[indexDistance] - uniqueDistance[indexDistance-1])) /denomArray[i];

			}


		}

		costs[0] = (travel.getters.cost() * passengers[0].distance) / (passengers.length);

		if(lastPassenger !== -1) {
			costs = costs.splice(0, lastPassenger)
		}

		return costs.reduce((acc, _) => acc+= _);



}

function proportionnalCost (state, passenger) {
	return ((travel.getters.totalCost() * (travel.getters.cost() * passenger.distance)) / travel.getters.sumAloneCosts())
}
