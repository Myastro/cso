
# Car Sharing
## Application to carpool cost sharing

### Abstract
This application is made to compute the cost of a travel sharing from 2 to 5 people. It is used to determinate what is the most beneficial, for a person who wants to travel from A to B, to share his travel between 1,2,3 or 4 people.
This application will display tables about costs if alone, stand-alone costs, proportional cost allocation and allocation by separation. It will also display if the given allocation satisfy the stand-alone test and if there is a core or not.

### Co-workers
Léo DUMON
Arnaud SOLER

# Configuration
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
